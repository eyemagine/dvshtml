$(document).ready(function() {

    $(".team#banner").jquerycycle({
         stopOnClick: false,
         "callback": function(cycle) {

            cycle.obj.find(".cycleNav").prepend($("<li></li>").addClass("cycleStartStop"));

            cycle.obj.find(".cycleStartStop").click(function() {

                if ($(this).hasClass("active")) {

                    cycle.options.cycle = true;
                    cycle.moveAll(1, false);
                }
                else cycle.options.cycle = false;

                $(this).toggleClass("active");
            });
         }
    });

    $(".member#banner .rotate").jquerycycle();

    $(".products").jquerycycle({
        lrControls: {
           enabled: true
        }
    });

    $(".person").hover(function() {

        $(this).find(".outline").stop(true, true).fadeOut(250, "linear");
    }, function() {

        $(this).find(".outline").stop(true, true).fadeIn(250, "linear");
    });

    $(".videoList li").click(function() {

        $(this).siblings(".active").removeClass("active");
        $(this).addClass("active");

        $(".videoPlayer").html('<iframe src="http://player.vimeo.com/video/' + $(this).attr("data-vimeo") + '" width="672" height="379" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>')
    });
});