var dropnavs = new Array();

$.fn.extend({

    dropNav: function(options) {

        $(this).each(function() {

            dropnavs.push(new navDropDown(this, options || {}));
        });
    }
});

function navDropDown(object, options) {

    this.object = $(object);
    this.options = {
        dropdown: ".dropdown",
        trigger:  ".trigger",
        hoverTimeout: 1,
        "width": false
    };

    $.extend(true, this.options, options);

    this.dd = this.object.find(this.options.dropdown);
    this.trigger = this.object.find(this.options.trigger);
    this.state = false;
    this.timer = 0;
    
    this.init();
}

navDropDown.prototype = {

    close: function(that) {

        that = that || this;

        that.object.removeClass("active");
        that.setState(false);
        that.dd.hide();
    },
    init: function() {

        this.dd.find("li").width(this.options.width || this.trigger.width());
        this.setEvents();
    },
    open: function() {

        this.object.addClass("active");
        this.setState(true);
        this.dd.show();
    },
    setEvents: function() {

        var that = this;

        that.trigger.hover(function() {

            clearTimeout(that.timer);

            that.open();
        }, function() {

            that.timer = setTimeout(function() {

                that.close();

            }, that.options.hoverTimeout);
        });

        that.dd.hover(function() {

            clearTimeout(that.timer);

            that.open();
        }, function() {

            that.timer = setTimeout(function() {

                that.close();

            }, that.options.hoverTimeout);
        });
    },
    setState: function(state) {

        return this.state = state;
    },
    toggleState: function() {

        return this.state = !this.state;
    }
}