$(document).ready(function () {

    $("#banner").jquerycycle({
         stopOnClick: false,
         callback: function(cycle) {

            cycle.obj.find(".cycleNav").prepend($("<li></li>").addClass("cycleStartStop"));

            cycle.obj.find(".cycleStartStop").click(function() {

                if ($(this).hasClass("active")) {

                    cycle.options.cycle = true;
                    cycle.moveAll(1, false);
                }
                else {

                    cycle.options.cycle = false;
                }

                $(this).toggleClass("active");
            });
         }
    });

    $(".products").jquerycycle({
         lrControls: {
            enabled: true
         }
    });
});