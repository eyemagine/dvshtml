$(document).ready(function() {

    $(".dropNav").each(function() {

        $(this).dropNav({
            "width": $(this).hasClass("extraWidth") ? 100 : false
        });
    })

    $("form .oneline-element input[type='text']").each(function() {

        var fontStyle = $(this).css("font-style");

        $(this).focus(function() {

            $(this).css("font-style", "normal");
        }).blur(function () {

            $(this).css("font-style", fontStyle);
        });
    });

    if ($.browser.msie) { // no one likes you ie

        $("[placeholder]").each(function() {

            var origValue = $(this).val();

            $(this).val(origValue != '' ? origValue : $(this).attr("placeholder"));

            $(this).focus(function() {

                if ($(this).val() == $(this).attr("placeholder")) $(this).val('');
            });

            $(this).blur(function() {

                if ($(this).val() == '') $(this).val(origValue != '' ? origValue : $(this).attr("placeholder"));
            });
        });
    }

    $(".overlayWrapper .overlay .bg").animate({opacity: 0.6}, 1);
    $(".overlayWrapper .overlay").animate({opacity: 0.0}, 1);

    $(".overlayWrapper").hover(function() {

        $(this).find(".overlay").stop().animate({opacity: 1}, 250, "linear");
    }, function() {

        $(this).find(".overlay").stop().animate({opacity: 0}, 250, "linear");
    });

    $(".superlink").click(function() {

        window.location = $(this).find("a[href]").attr("href");
    });
});